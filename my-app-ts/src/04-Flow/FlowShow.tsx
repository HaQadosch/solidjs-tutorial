import { Component, createSignal, Show } from 'solid-js'

export const FlowShow: Component = () => {
  const [loggedIn, setLoggedIn] = createSignal<boolean>(false)

  const toggleLogIn = (): void => {
    console.log({ status: loggedIn() })
    setLoggedIn(status => !status)
  }

  return (
    <>
      {loggedIn()
        ? <button onClick={toggleLogIn}>Log Out</button>
        : <button onClick={toggleLogIn}>Log In</button>}
      <Show
        when={loggedIn()}
        fallback={
          () => <button onClick={toggleLogIn}>Show Log In</button>
        }
      >
        <button onClick={toggleLogIn}>Show Log Out</button>
      </Show>
    </>
  )
}
