import { Component, createSignal, For } from 'solid-js'

interface TCat {
  id: string
  name: string
}

export const FlowFor: Component = () => {
  const [cats] = createSignal<TCat[]>([
    { id: 'J---aiyznGQ', name: 'Keyboard Cat' },
    { id: 'z_AbfPXTKms', name: 'Maru' },
    { id: 'OUtn3pvWmpg', name: 'Henri The Existential Cat' }
  ])

  return (
    <ul>
      {cats().map((cat, index) => (
        <li>
          <a href={`https://www.youtube.com/watch?v=${cat.id}`}>{`${index + 1}: ${cat.name}`}</a>
        </li>
      ))}
      <For each={cats()}>
        {(cat, i) => (
          <li>
            <a href={`https://www.youtube.com/watch?v=${cat.id}`}>{`${i() + 1}: ${cat.name}`}</a>
          </li>
        )}
      </For>
    </ul>
  )
}
