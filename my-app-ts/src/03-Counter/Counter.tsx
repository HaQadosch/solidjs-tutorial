import { Component, createEffect, createMemo, createSignal } from 'solid-js'

const fibo = (x: number): number => x <= 1 ? 1 : (fibo(x - 1) + fibo(x - 2))

export const Counter: Component = () => {
  const [count, setCount] = createSignal<number>(0)

  const fib = createMemo(() => {
    console.count('fibbing again')
    return fibo(count())
  })

  createEffect(() => {
    console.log({ count: count() })
  })

  const double = (): number => count() * 2

  return (
    <article>
      <p>
        {`Count ${double()}`}
        {fib()} {fib()} {fib()} {fib()} {fib()}
      </p>
      <button onClick={() => { setCount(c => c + 1) }}>+ 1</button>
    </article>
  )
}
