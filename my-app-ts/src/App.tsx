import type { Component } from 'solid-js'
import { Hello } from './01/Hello'
import { FlowShow } from './04-Flow/FlowShow'
import { FlowFor } from './04-Flow/FlowFor'
import logo from './logo.svg'
import styles from './App.module.css'
import { Svg } from './02/SVG'
import { Counter } from './03-Counter/Counter'

const App: Component = () => {
  return (
    <div class={styles.App}>
      <header class={styles.header}>
        <img src={logo} class={styles.logo} alt='logo' />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
          <br />
          <FlowFor />
          <FlowShow />
          <Counter />
          <Svg />
          <Hello />
        </p>
        <a
          class={styles.link}
          href='https://github.com/ryansolid/solid'
          target='_blank'
          rel='noopener noreferrer'
        >
          Learn Solid
        </a>
      </header>
    </div>
  )
}

export default App
