import type { Component } from 'solid-js'

export const Hello: Component = () => {
  return (
    <main>Hello World!</main>
  )
}
