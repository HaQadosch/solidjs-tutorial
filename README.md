# SolidJS Tutorial

As found in https://www.solidjs.com/tutorial/introduction_basics 

# Template for SolidJS project

```shell
npx degit solidjs/templates/js my-app # for JS
npx degit solidjs/templates/ts my-app # for TS
cd my-app/
npm i
```

# Rendering

From Solid’s perspective all rendering is just a side effect of the reactive system.

Effects that developers create with `createEffect` run after rendering has completed and are mostly used for scheduling post render updates that interact with the DOM.